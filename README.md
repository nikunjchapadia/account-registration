Requirements
Client must be written in HTML/JavaScript - any library can be used, but are optional
Server must be written in Java (Example starting Java class has been included below)

#Task 
* Setup server using HttpServer
* Figure out how many diff operation we need - get, post etc ..
* Create html page with 4 fields (start with just one and later add rest of things) 
* Render html page 
* Post form data 
* Create object out of form data
* Temp put in memory and later i will implement simple db or just stor in file on disk
* Look and feel : add some css to make it presentable 

* now lets do some client and server validation 

* Validation : Add minmum validation on client side 
* Validation : Server side validation 
* Validation : use given images for sucess and failure

* Communicate : communicate error with users
* Test : add minmum test for all operations 
* Think about n number of users and concurrecny
* Discover things which is not listed here 

* Many things we can do after this 
** ---


# Stories
* Server setup
* As a user, I would like to be able to load an HTML file from the SimpleServer, e.g. http://localhost:8756/register

* Form creation  
* As a user, I would like to be able to enter in the following four fields and have them sent to the server by selecting
 a “Create Account” button:
** username
** email_address
** password
** zipcode
* Server side validation 
* As a user, I would like the username validated when the username field loses focus  (must be a unique username; validated by the server):
** if valid, a green colored checkmark will be placed next to the field (see image below)
** if invalid, a red colored checkmark will be placed next to the field (see image below)
* Client side validation 
* As a user, I would like the “Create Account” button to not submit if the username field is not valid
* Server side validation
* As a user, I would like to see an error message next to any/all of the fields if they are not valid (validated by the server).
  Use the red colored checkmark image (below) and the following error text/validation
** username - “username is not unique”
** email_address - “invalid email address”
** password - “password too short, must be more than 6 characters”
** zipcode - “zipcode is incorrect, must only contain 5 digits”
* Data store
As the user, I would like the my registration stored on the server (transient memory ok)

** 
Extra/Stretch Goals
Use a database for actual storage (persistent across restarts)
Create a duplicate version of the server in Node.JS
