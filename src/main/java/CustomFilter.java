package main.java;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <code>CustomFilter</code>
 *
 * @author Nikunj Chapadia
 * @version 7/11/15 2:01 PM (11 Jul 2015)
 */
public class CustomFilter extends Filter {

    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        Map<String, Object> params = new HashMap<String, Object>();
        System.out.println(httpExchange.getRequestURI());
        URI uri = httpExchange.getRequestURI();
        String str = uri.getRawQuery();
        processData(str,params);
        httpExchange.setAttribute("parameters", params);
//        if (RequestMethod.GET.toString().equals(httpExchange.getRequestMethod())) {
//        }
        if (RequestMethod.POST.toString().equals(httpExchange.getRequestMethod())) {
            Map<String, Object> params1 = (Map<String, Object>) httpExchange.getAttribute("parameters");
            InputStreamReader inputStreamReader = new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String str1 = bufferedReader.readLine();
            processData(str1,params1);
        }
        chain.doFilter(httpExchange);
    }

    @Override
    public String description() {
        return "description";
    }

    private void processData(String str, Map<String, Object> params)
            throws UnsupportedEncodingException {
        if (str != null && str.trim().length() > 0) {
            // first split for individual params
            String[] list = str.split("&");
            for (String keyValue : list) {
                // second split for key and value
                String[] obj = keyValue.split("=");
                String key = null,
                        value = null;
                if (obj != null && obj.length > 0 && obj[0] != null) {
                    key = URLDecoder.decode(obj[0], "UTF-8");
                }
                if (obj != null && obj.length > 0 && obj[1] != null) {
                    value = URLDecoder.decode(obj[1], "UTF-8");
                }
                if (params.containsKey(key)) {
                    Object keyObj = params.get(key);
                    if (keyObj instanceof List<?>) {
                        List<String> values = (List<String>) keyObj;
                        values.add(value);
                    } else if (keyObj instanceof String) {
                        List<String> values = new ArrayList<String>();
                        values.add((String) keyObj);
                        values.add(value);
                        params.put(key, values);
                    }
                } else {
                    params.put(key, value);
                }
            }
        }
    }
}
