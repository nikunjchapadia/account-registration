package main.java;

import com.sun.net.httpserver.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;


public class SimpleServer {

    private static final String ONLY_USERNAME = "onlyUsername";
    static Map<String, UserAccount> userAccountMap;

    static {
        // other stuff might needed at class init time
        userAccountMap = new HashMap<String, UserAccount>();
    }

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8765), 0);
        // handle / because it will throw 404
        HttpContext context = server.createContext("/register", new RegistrationHandler());
        context.getFilters().add(new CustomFilter());
        HttpContext validationContext = server.createContext("/validate", new ValidationHandler());
        validationContext.getFilters().add(new CustomFilter());
        server.createContext("/images", new ImageHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
        // need to implement stop and other lifecycle
        System.out.println("Server : " + server.getAddress());
    }

    static class RegistrationHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            // next step
            // first thing figure out how to render html page
            // second figure out how to handle all the methods

            // lets first create html file - register.html
            // now get this file and create response body
            // handle many exceptions here related to file reading and writing

            // handle each method seperatly
            String method = httpExchange.getRequestMethod();
            System.out.println("Method : " + method);
            Headers headers = httpExchange.getResponseHeaders();
            if (RequestMethod.GET.toString().equals(method)) {
                headers.set("Content-Type", "text/html");
                httpExchange.sendResponseHeaders(200, 0);
                String file = new File("register.html").getAbsolutePath();
                writeFile(httpExchange,file);
            }
            if (RequestMethod.POST.toString().equals(method)) {
                System.out.println("Handling post method");
                Map<String, Object> params = (Map<String, Object>) httpExchange.getAttribute("parameters");
                UserAccount account = new UserAccount();
                account.setUsername(params.get("username").toString());
                account.setPassword(params.get("password").toString());
                account.setEmail(params.get("email").toString());
                account.setZip(getZip(params.get("zip").toString()));
                if (!userAccountMap.containsKey(account.getUsername())) {
                    userAccountMap.put(account.getUsername(), account);
                }
                printAllAccounts();
            }
        }
    }

    static class ValidationHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String method = httpExchange.getRequestMethod();
            if (RequestMethod.GET.toString().equals(method)) {
                System.out.println("Validate");
                Map<String, Object> params = (Map<String, Object>) httpExchange.getAttribute("parameters");
                if (params == null) {
                    return;
                }
                String only = (String) params.get(ONLY_USERNAME);
                Boolean onlyUsername = Boolean.valueOf(only);
                String username = (String) params.get("username");
                System.out.println("Username : " + username);
                Map<String, String> messages = new HashMap<String, String>();
                if (username == null || username.length() == 0) {
                    messages.put("username", "No username found");
                } else if (username.length() <= 4) {
                    messages.put("username", "username must be more than four chars");
                } else if (userAccountMap.containsKey(username)) {
                    messages.put("username", "username is not unique");
                }
                if (!onlyUsername) {
                    System.out.println("Validate complete form");
                    // validate password
                    String password = (String) params.get("password");
                    if (isEmpty(password)) {
                        messages.put("password", "password is empty");
                    } else if (password.length() <= 6) {
                        messages.put("password", "password too short - must be more than 6 characters");
                    }
                    // validate email - its easy to write rege exp lets do it later
                    String email = (String) params.get("email");
                    if (isEmpty(email)) {
                        messages.put("email", "invalid email address");
                    }
                    String zip = (String) params.get("zip");
                    // zipcode is incorrect, must only contain 5 digits
                    if (isEmpty(zip)) {
                        messages.put("zip", "invalid zip code");
                    } else if (zip.length() != 5 || getZip(zip) == null) {
                        messages.put("zip", "zip code is incorrect - must only contain 5 digits");
                    }
                }
                httpExchange.sendResponseHeaders(200, mapToString(messages).length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(mapToString(messages).getBytes());
                os.close();
            }
        }
    }

    static class ImageHandler implements HttpHandler{

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String method = httpExchange.getRequestMethod();
            System.out.println("Method : " + method);
            Headers headers = httpExchange.getResponseHeaders();
            headers.set("Content-Type", "image/png");
            httpExchange.sendResponseHeaders(200, 0);
            try {
                if (httpExchange.getRequestURI().toString().equals("/images/image00.png")) {
                    String file1 = new File("images/image00.png").getAbsolutePath();
                    writeFile(httpExchange, file1);
                }
                if (httpExchange.getRequestURI().toString().equals("/images/image01.png")) {
                    String file2 = new File("images/image01.png").getAbsolutePath();
                    writeFile(httpExchange, file2);
                }
            } catch (IOException e) {
                System.out.println("Error reading file data" + e.getMessage());
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public static void writeFile(HttpExchange httpExchange, String file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        final byte[] buffer = new byte[0x10000];
        int count = 0;
        OutputStream outputStream = httpExchange.getResponseBody();
        while ((count = fileInputStream.read(buffer)) >= 0) {
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
    }

    private static void printAllAccounts() {
        for (UserAccount account : userAccountMap.values()) {
            System.out.println(account.toString());
        }
    }

    private static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    private static Integer getZip(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            System.out.println("Zip contains invalid chars");
        }
        return null;
    }

    private static String mapToString(Map map) {
        if (map == null) {
            return null;
        }
        StringBuilder s = new StringBuilder();
        for (Object key : map.keySet()) {
            if (s.length() > 0) {
                s.append(",");
            }
            s.append(key + ":" + map.get(key));
        }
        return s.toString();
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

}

